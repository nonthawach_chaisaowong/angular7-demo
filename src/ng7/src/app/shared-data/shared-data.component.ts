import { Component, OnInit } from '@angular/core';
import { MessageseService } from '../messagese.service';

@Component({
  selector: 'app-shared-data',
  templateUrl: './shared-data.component.html',
  styleUrls: ['./shared-data.component.scss']
})
export class SharedDataComponent implements OnInit {


  parentMessage = 'message from parent from property';
  // sencond child
  secondchildmessage: string;
  // message from service
  messageFromService: string;

  constructor(private messageService: MessageseService) { }

  ngOnInit() {
    this.messageService.currentMessage.subscribe(messageFromService => this.messageFromService = messageFromService);
  }

  // sencond child
  receiveMessage($event) {
    this.secondchildmessage = $event;
  }

  // broadcastMessage
  broadcastMessage(message) {
    this.messageService.changeMessage(message);
  }

}

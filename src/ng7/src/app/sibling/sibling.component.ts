import { Component, OnInit } from '@angular/core';
import { MessageseService } from '../messagese.service';

@Component({
  selector: 'app-sibling',
  templateUrl: './sibling.component.html',
  styleUrls: ['./sibling.component.scss']
})
export class SiblingComponent implements OnInit {

   // message from service
   messageFromService: string;

   constructor(private messageService: MessageseService) { }

   ngOnInit() {
    this.messageService.currentMessage.subscribe(messageFromService => this.messageFromService = messageFromService);
   }
    // broadcastMessage
    broadcastMessage(message) {
      this.messageService.changeMessage(message);
    }

}

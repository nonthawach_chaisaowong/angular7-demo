import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-secondchild',
  templateUrl: './secondchild.component.html',
  styleUrls: ['./secondchild.component.scss']
})
export class SecondchildComponent implements OnInit {

  @Output() messageEvent = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  sendMessage(inputvalue: string) {
    this.messageEvent.emit(inputvalue);
  }

}

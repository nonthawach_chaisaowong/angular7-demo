# Ng7

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

## How to run 

1. Download code code from repo
2. Go to `...\src\ng7` folder
3. Run `npm install`
4. Run `ng serve`


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
